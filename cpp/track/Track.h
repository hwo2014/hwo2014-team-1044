#ifndef TRACK_H__
#define TRACK_H__

#include <iostream>
#include <vector>
#include <map>
#include <jsoncons/json.hpp>
#include "TrackPiece.h"
#include "../cars/CarPosition.h"

class Track {
public:
	Track();
	Track(const Track&);
	Track(const jsoncons::json&);
	virtual ~Track();

	int switchLane(const CarPosition&);
	double throttle(const CarPosition&) const;

private:
	typedef struct {
		int m_pieceIndex = -1;
		int m_laneIndex = -1;
		int m_lap = -1;
	} SwitchPosition;

	std::string m_id;
	std::string m_name;
	std::map<int, double> m_lanes;
	std::vector<TrackPiece> m_pieces;
	std::vector<SwitchPosition> m_switches;
};

#endif
