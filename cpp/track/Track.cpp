#include "Track.h"

Track::Track() {
}

Track::Track(const Track& track) {
	m_id = track.m_id;
	m_name = track.m_name;
	m_lanes = track.m_lanes;
	m_pieces = track.m_pieces;
}

Track::Track(const jsoncons::json& track) {
	m_id = track["id"].as<std::string>();
	m_name = track["name"].as<std::string>();

	const auto& lanes = track["lanes"];
	for (size_t index = 0; index < lanes.size(); ++index) {
		const auto& lane = lanes[index];
		m_lanes[lane["index"].as_int()] =
				lane["distanceFromCenter"].as_double();
	}

	const auto& pieces = track["pieces"];
	for (size_t index = 0; index < pieces.size(); ++index) {
		m_pieces.push_back(TrackPiece(pieces[index]));
	}

	int laneMinIndex = 0;
	int laneMaxIndex = m_lanes.size() - 1;
	double laneMinDist = m_lanes[laneMinIndex];
	double laneMaxDist = m_lanes[laneMaxIndex];
	for (size_t startIndex = 0; startIndex < m_pieces.size(); ++startIndex) {
		const TrackPiece& startPiece = m_pieces[startIndex];
		if (startPiece.isSwitch()) {
			double laneMinLength = 0;
			double laneMaxLength = 0;
			for (size_t endIndex = 0; endIndex < m_pieces.size(); ++endIndex) {
				const TrackPiece& endPiece =
						m_pieces[(startIndex + 1 + endIndex) % m_pieces.size()];
				if (endPiece.isSwitch()) {
					SwitchPosition switchPosition;
					switchPosition.m_lap = -1;
					switchPosition.m_pieceIndex = startIndex;
					switchPosition.m_laneIndex =
							laneMinLength < laneMaxLength ?
									laneMinIndex : laneMaxIndex;
					m_switches.push_back(switchPosition);
					break;
				} else {
					laneMinLength += endPiece.length(laneMinDist);
					laneMaxLength += endPiece.length(laneMaxDist);
				}
			}
		}
	}
}

Track::~Track() {
}

int Track::switchLane(const CarPosition& position) {
	for (size_t index = 0; index < m_switches.size(); ++index) {
		if (m_switches[index].m_pieceIndex >= position.pieceIndex()) {
			if (m_switches[index].m_lap != position.lap()) {
				m_switches[index].m_lap = position.lap();
				std::cout << "switch="
						<< m_switches[index].m_laneIndex
								- position.endLaneIndex() << std::endl;
				return m_switches[index].m_laneIndex - position.endLaneIndex();
			}
			break;
		}
	}
	return 0;
}

double Track::throttle(const CarPosition& position) const {
	double throttle = 0.5;
	return throttle;
}
