#include "TrackPiece.h"

TrackPiece::TrackPiece(const jsoncons::json& piece) {
	if (piece.has_member("length")) {
		m_length = piece["length"].as_double();
		m_angle = m_radius = 0;
		m_type = STRAIGHT;
	} else {
		m_angle = piece["angle"].as_double();
		m_radius = piece["radius"].as_double();
		m_type = CURVE;
	}
	if (piece.has_member("switch")) {
		m_switch = piece["switch"].as_bool();
	} else {
		m_switch = false;
	}
}

TrackPiece::~TrackPiece() {
}

TrackPiece::PieceType TrackPiece::type() const {
	return m_type;
}

double TrackPiece::length(double offset) const {
	if (m_type == STRAIGHT) {
		return m_length;
	}
	if (m_type == CURVE) {
		offset = m_angle < 0 ? offset : -offset;
		return 2 * M_PI * (m_radius + offset) * abs(m_angle) / 360;
	}
	return 0;
}

bool TrackPiece::isSwitch() const {
	return m_switch;
}
