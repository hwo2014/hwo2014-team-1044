#ifndef TRACKPIECE_H__
#define TRACKPIECE_H__

#include <iostream>
#include <jsoncons/json.hpp>

class TrackPiece {
public:
	enum PieceType {
		STRAIGHT, CURVE
	};

	TrackPiece(const jsoncons::json&);
	virtual ~TrackPiece();

	PieceType type() const;
	double length(double offset = 0) const;
	bool isSwitch() const;

private:

	PieceType m_type;
	double m_length;
	double m_angle;
	double m_radius;
	bool m_switch;
};

#endif
