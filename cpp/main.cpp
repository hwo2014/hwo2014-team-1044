#include <iostream>
#include <string>
#include <jsoncons/json.hpp>
#include "game/Protocol.h"
#include "game/Connection.h"
#include "game/GameLogic.h"

void run(Connection& connection, const std::string& name,
		const std::string& key) {
	GameLogic game;
	connection.sendRequests( { Protocol::makeJoin(name, key) });
	while (true) {
		boost::system::error_code error;
		auto response = connection.receiveResponse(error);
		if (error == boost::asio::error::eof) {
			std::cout << "Connection closed" << std::endl;
			break;
		} else if (error) {
			throw boost::system::system_error(error);
		}
		connection.sendRequests(game.react(response));
	}
}

int main(int argc, const char* argv[]) {
	if (argc != 5) {
		std::cerr << "Usage: ./run host port botname botkey" << std::endl;
		return 1;
	}
	try {
		const std::string host(argv[1]);
		const std::string port(argv[2]);
		const std::string name(argv[3]);
		const std::string key(argv[4]);
		std::cout << "Host: " << host << ", port: " << port << ", name: "
				<< name << ", key:" << key << std::endl;
		Connection connection(host, port);
		run(connection, name, key);
	} catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return 2;
	}
	return 0;
}
