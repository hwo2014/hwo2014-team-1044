#include "GameLogic.h"
#include "Protocol.h"

GameLogic::GameLogic() {
	m_actionMap["join"] = &GameLogic::on_join;
	m_actionMap["yourCar"] = &GameLogic::on_yourCar;
	m_actionMap["gameInit"] = &GameLogic::on_gameInit;
	m_actionMap["gameStart"] = &GameLogic::on_gameStart;
	m_actionMap["carPositions"] = &GameLogic::on_carPositions;
	m_actionMap["crash"] = &GameLogic::on_crash;
	m_actionMap["gameEnd"] = &GameLogic::on_gameEnd;
	m_actionMap["error"] = &GameLogic::on_error;
}

GameLogic::msg_vector GameLogic::react(const jsoncons::json& msg) {
	const auto& msg_type = msg["msgType"].as<std::string>();
	auto action_it = m_actionMap.find(msg_type);
	if (action_it != m_actionMap.end()) {
		return (action_it->second)(this, msg);
	} else {
		std::cout << "Unknown message type: " << msg_type << std::endl;
		return {Protocol::makePing()};
	}
}

GameLogic::msg_vector GameLogic::on_join(const jsoncons::json& msg) {
	std::cout << "Joined" << std::endl;
	return {Protocol::makePing()};
}

GameLogic::msg_vector GameLogic::on_yourCar(const jsoncons::json& msg) {
	std::cout << "Your Car :: ";
	const auto& data = msg["data"];
	CarId carId(data);
	m_cars.setSelf(carId);
	std::cout << carId << std::endl;
	return {Protocol::makePing()};
}

GameLogic::msg_vector GameLogic::on_gameInit(const jsoncons::json& msg) {
	std::cout << "Game Init ::";
	const auto& data = msg["data"];
	const auto& race = data["race"];
	const auto& track = race["track"];
	const auto& cars = race["cars"];
	m_track = Track(track);
	std::cout << " count=" << cars.size() << " ::";
	for (size_t index = 0; index < cars.size(); ++index) {
		Car car(cars[index]);
		m_cars.addCar(car);
		std::cout << " " << car;
	}
	std::cout << std::endl;
	return {Protocol::makePing()};
}

GameLogic::msg_vector GameLogic::on_gameStart(const jsoncons::json& msg) {
	std::cout << "Race started" << std::endl;
	return {Protocol::makePing()};
}

GameLogic::msg_vector GameLogic::on_carPositions(const jsoncons::json& msg) {
	msg_vector result = { Protocol::makePing() };
	const auto& data = msg["data"];
	for (size_t index = 0; index < data.size(); ++index) {
		const auto& position = data[index];
		const auto& id = position["id"];
		CarId carId(id);
		CarPosition carPosition(position);
		if (m_cars.hasCar(carId)) {
			m_cars.getCar(carId).setPosition(carPosition);
			if (m_cars.isSelf(carId)) {
				int switchLane = m_track.switchLane(carPosition);
				if (switchLane < 0) {
					result = {Protocol::makeSwitchLane("Left")};
				} else if (switchLane > 0) {
					result = {Protocol::makeSwitchLane("Right")};
				} else {
					double throttle = m_track.throttle(carPosition);
					result = {Protocol::makeThrottle(throttle)};
				}
			}
		}
	}
	return result;
}

GameLogic::msg_vector GameLogic::on_crash(const jsoncons::json& msg) {
	std::cout << "Someone crashed" << std::endl;
	return {Protocol::makePing()};
}

GameLogic::msg_vector GameLogic::on_gameEnd(const jsoncons::json& msg) {
	std::cout << "Race ended" << std::endl;
	return {Protocol::makePing()};
}

GameLogic::msg_vector GameLogic::on_error(const jsoncons::json& msg) {
	const auto& data = msg["data"];
	std::cout << "Error: " << data.to_string() << std::endl;
	return {Protocol::makePing()};
}
