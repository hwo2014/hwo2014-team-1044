#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "../cars/Cars.h"
#include "../track/Track.h"

class GameLogic {
public:
	typedef std::vector<jsoncons::json> msg_vector;

	GameLogic();
	msg_vector react(const jsoncons::json& msg);

private:
	typedef std::function<msg_vector(GameLogic*, const jsoncons::json&)> action_func;
	std::map<std::string, action_func> m_actionMap;

	msg_vector on_join(const jsoncons::json&);
	msg_vector on_yourCar(const jsoncons::json&);
	msg_vector on_gameInit(const jsoncons::json&);
	msg_vector on_gameStart(const jsoncons::json&);
	msg_vector on_carPositions(const jsoncons::json&);
	msg_vector on_crash(const jsoncons::json&);
	msg_vector on_gameEnd(const jsoncons::json&);
	msg_vector on_error(const jsoncons::json&);

	Cars m_cars;
	Track m_track;
};

#endif
