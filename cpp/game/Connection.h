#ifndef CONNECTION_H__
#define CONNECTION_H__

#include <string>
#include <iostream>
#include <boost/asio.hpp>
#include <jsoncons/json.hpp>

using boost::asio::ip::tcp;

class Connection
{
public:
  Connection(const std::string& host, const std::string& port);
  virtual ~Connection();
  jsoncons::json receiveResponse(boost::system::error_code& error);
  void sendRequests(const std::vector<jsoncons::json>& msgs);

private:
  boost::asio::io_service io_service;
  tcp::socket socket;
  boost::asio::streambuf response_buf;
};

#endif
