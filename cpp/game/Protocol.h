#ifndef PROTOCOL_H__
#define PROTOCOL_H__

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>

class Protocol {
public:
	static jsoncons::json makeRequest(const std::string&,
			const jsoncons::json&);
	static jsoncons::json makeJoin(const std::string&, const std::string&);
	static jsoncons::json makePing();
	static jsoncons::json makeThrottle(double throttle);
	static jsoncons::json makeSwitchLane(const std::string&);
};

#endif
