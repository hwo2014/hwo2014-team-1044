#include "Protocol.h"

jsoncons::json Protocol::makeRequest(const std::string& msg_type,
		const jsoncons::json& data) {
	jsoncons::json r;
	r["msgType"] = msg_type;
	r["data"] = data;
	return r;
}

jsoncons::json Protocol::makeJoin(const std::string& name,
		const std::string& key) {
	jsoncons::json data;
	data["name"] = name;
	data["key"] = key;
	return makeRequest("join", data);
}

jsoncons::json Protocol::makePing() {
	return makeRequest("ping", jsoncons::null_type());
}

jsoncons::json Protocol::makeThrottle(double throttle) {
	return makeRequest("throttle", throttle);
}

jsoncons::json Protocol::makeSwitchLane(const std::string& direction) {
	return makeRequest("switchLane", direction);
}
