#include "CarId.h"

CarId::CarId() {
}

CarId::CarId(const jsoncons::json& id) {
	m_name = id["name"].as<std::string>();
	m_color = id["color"].as<std::string>();
}

CarId::~CarId() {
}

const std::string& CarId::name() const {
	return m_name;
}

const std::string& CarId::color() const {
	return m_color;
}

std::ostream& operator<<(std::ostream& os, const CarId& self) {
	os << "name=" << self.m_name;
	os << " color=" << self.m_color;
	return os;
}
