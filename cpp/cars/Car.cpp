#include "Car.h"

Car::Car() {
}

Car::Car(const Car& car) :
		m_id(car.m_id), m_dimensions(car.m_dimensions) {
}

Car::Car(const jsoncons::json& car) :
		m_id(car["id"]), m_dimensions(car["dimensions"]) {
}

Car::~Car() {
}

const CarId& Car::id() const {
	return m_id;
}

const CarDimensions& Car::dimensions() const {
	return m_dimensions;
}

const CarPosition& Car::position() const {
	return m_position;
}

void Car::setPosition(const CarPosition& position) {
	m_position = position;
}

std::ostream& operator<<(std::ostream& os, const Car& self) {
	os << "id:: " << self.m_id << "  dimensions:: " << self.m_dimensions << "  position:: " << self.m_position;
	return os;
}
