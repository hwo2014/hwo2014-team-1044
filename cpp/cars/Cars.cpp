#include "Cars.h"

Cars::Cars() {
}

Cars::~Cars() {
}

bool Cars::isSelf(const CarId& carId) const {
	return m_self.color() == carId.color();
}

void Cars::setSelf(const CarId& self) {
	m_self = self;
}

void Cars::addCar(const Car& car) {
	m_carMap[car.id().color()] = car;
}

bool Cars::hasCar(const CarId& carId) const {
	auto it = m_carMap.find(carId.color());
	return it != m_carMap.end();
}

Car& Cars::getCar(const CarId& carId) {
	return m_carMap.find(carId.color())->second;
}
