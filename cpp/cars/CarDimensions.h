#ifndef CARDIMENSIONS_H__
#define CARDIMENSIONS_H__

#include <iostream>
#include <jsoncons/json.hpp>

class CarDimensions {
public:
	CarDimensions();
	CarDimensions(const CarDimensions&);
	CarDimensions(const jsoncons::json&);
	virtual ~CarDimensions();

	friend std::ostream& operator<<(std::ostream&, const CarDimensions&);

private:
	double m_length;
	double m_width;
	double m_guideFlagPosition;
};

#endif /* CARDIMENSIONS_H__ */
