#ifndef CARS_H__
#define CARS_H__

#include <string>
#include <map>
#include "Car.h"

class Cars {
public:
	Cars();
	virtual ~Cars();

	bool isSelf(const CarId&) const;
	void setSelf(const CarId&);
	void addCar(const Car&);
	bool hasCar(const CarId&) const;
	Car& getCar(const CarId&);

private:
	CarId m_self;
	std::map<std::string, Car> m_carMap;
};

#endif
