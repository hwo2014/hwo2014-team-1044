#ifndef CARID_H__
#define CARID_H__

#include <iostream>
#include <string>
#include <jsoncons/json.hpp>

class CarId {
public:
	CarId();
	CarId(const jsoncons::json&);
	virtual ~CarId();

	const std::string& name() const;
	const std::string& color() const;

	friend std::ostream& operator<<(std::ostream&, const CarId&);

private:
	std::string m_name;
	std::string m_color;
};

#endif /* CARID_H__ */
