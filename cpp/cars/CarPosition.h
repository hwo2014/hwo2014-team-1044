#ifndef CARPOSITION_H__
#define CARPOSITION_H__

#include <iostream>
#include <jsoncons/json.hpp>

class CarPosition {
public:
	CarPosition();
	CarPosition(const CarPosition&);
	CarPosition(const jsoncons::json&);
	virtual ~CarPosition();

	double angle() const;
	int pieceIndex() const;
	double inPieceDistance() const;
	int startLaneIndex() const;
	int endLaneIndex() const;
	int lap() const;

	friend std::ostream& operator<<(std::ostream&, const CarPosition&);

private:
	double m_angle;
	int m_pieceIndex;
	double m_inPieceDistance;
	int m_startLaneIndex;
	int m_endLaneIndex;
	int m_lap;
};

#endif
