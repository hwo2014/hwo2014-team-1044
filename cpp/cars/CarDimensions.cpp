#include "CarDimensions.h"

CarDimensions::CarDimensions() {
	m_length = 0;
	m_width = 0;
	m_guideFlagPosition = 0;
}

CarDimensions::CarDimensions(const CarDimensions& carDimensions) {
	m_length = carDimensions.m_length;
	m_width = carDimensions.m_width;
	m_guideFlagPosition = carDimensions.m_guideFlagPosition;
}

CarDimensions::CarDimensions(const jsoncons::json& dimensions) {
	m_length = dimensions["length"].as_double();
	m_width = dimensions["width"].as_double();
	m_guideFlagPosition = dimensions["guideFlagPosition"].as_double();
}

CarDimensions::~CarDimensions() {
}

std::ostream& operator<<(std::ostream& os, const CarDimensions& self) {
	os << "length=" << self.m_length;
	os << " width=" << self.m_width;
	os << " guideFlagPosition=" << self.m_guideFlagPosition;
	return os;
}
