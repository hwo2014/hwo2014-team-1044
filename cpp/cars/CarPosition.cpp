#include "CarPosition.h"

CarPosition::CarPosition() {
	m_angle = 0;
	m_pieceIndex = 0;
	m_inPieceDistance = 0;
	m_startLaneIndex = 0;
	m_endLaneIndex = 0;
	m_lap = 0;
}

CarPosition::CarPosition(const CarPosition& carPosition) {
	m_angle = carPosition.m_angle;
	m_pieceIndex = carPosition.m_pieceIndex;
	m_inPieceDistance = carPosition.m_inPieceDistance;
	m_startLaneIndex = carPosition.m_startLaneIndex;
	m_endLaneIndex = carPosition.m_endLaneIndex;
	m_lap = carPosition.m_lap;
}

CarPosition::CarPosition(const jsoncons::json& position) {
	const auto& piecePosition = position["piecePosition"];
	const auto& lane = piecePosition["lane"];

	m_angle = position["angle"].as_double();
	m_pieceIndex = piecePosition["pieceIndex"].as_int();
	m_inPieceDistance = piecePosition["inPieceDistance"].as_double();
	m_startLaneIndex = lane["startLaneIndex"].as_int();
	m_endLaneIndex = lane["endLaneIndex"].as_int();
	m_lap = piecePosition["lap"].as_int();
}

CarPosition::~CarPosition() {
}

double CarPosition::angle() const {
	return m_angle;
}

int CarPosition::pieceIndex() const {
	return m_pieceIndex;
}

double CarPosition::inPieceDistance() const {
	return m_inPieceDistance;
}

int CarPosition::startLaneIndex() const {
	return m_startLaneIndex;
}

int CarPosition::endLaneIndex() const {
	return m_endLaneIndex;
}

int CarPosition::lap() const {
	return m_lap;
}

std::ostream& operator<<(std::ostream& os, const CarPosition& carPosition) {
	os << "angle=" << carPosition.m_angle;
	os << " pieceIndex=" << carPosition.m_pieceIndex;
	os << " inPieceDistance=" << carPosition.m_inPieceDistance;
	os << " startLane=" << carPosition.m_startLaneIndex;
	os << " endLane=" << carPosition.m_endLaneIndex;
	os << " lap=" << carPosition.m_lap;
	return os;
}
