#ifndef CAR_H__
#define CAR_H__

#include <iostream>
#include <jsoncons/json.hpp>
#include "CarId.h"
#include "CarDimensions.h"
#include "CarPosition.h"

class Car {
public:
	Car();
	Car(const Car&);
	Car(const jsoncons::json&);
	virtual ~Car();

	const CarId& id() const;
	const CarDimensions& dimensions() const;
	const CarPosition& position() const;

	void setPosition(const CarPosition&);

	friend std::ostream& operator<<(std::ostream&, const Car&);

private:
	CarId m_id;
	CarDimensions m_dimensions;
	CarPosition m_position;
};

#endif /* CAR_H__ */
